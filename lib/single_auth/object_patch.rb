module SingleAuth
  module ObjectPatch
    def self.included(base)
      base.send(:include, InstanceMethods)
    end

    module InstanceMethods
      def redef_without_warning(mod, const, value)
        mod = mod.is_a?(Module) ? mod : mod.class
        mod.send(:remove_const, const) if mod.const_defined?(const)
        mod.const_set(const, value)
      end
    end
  end
end